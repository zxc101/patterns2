using UnityEngine;

namespace Structural
{
    namespace Flyweight
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                var factory = new FlyweightFactory(
                    new Car { Company = "Chevralet", Model = "Camaro2018", Color = "pink" },
                    new Car { Company = "Mercedes Benz", Model = "C300", Color = "black" },
                    new Car { Company = "Mercedes Benz", Model = "C500", Color = "red" },
                    new Car { Company = "BMW", Model = "M5", Color = "red" },
                    new Car { Company = "BMW", Model = "M6", Color = "white" });

                print(factory.ListFlyweights());

                AddCarToPoliceDatabase(factory, new Car
                {
                    Number = "CL234IR",
                    Owner = "James Doe",
                    Company = "BMW",
                    Model = "M5",
                    Color = "red",
                });

                AddCarToPoliceDatabase(factory, new Car
                {
                    Number = "CL234IR",
                    Owner = "James Doe",
                    Company = "BMW",
                    Model = "X1",
                    Color = "red",
                });

                print(factory.ListFlyweights());
            }

            public static void AddCarToPoliceDatabase(FlyweightFactory factory, Car car)
            {
                print("Client: Adding a car to database.\n");

                var flyweight = factory.GetFlyweight(new Car
                {
                    Color = car.Color,
                    Model = car.Model,
                    Company = car.Company
                });

                flyweight.Operation(car);
            }
        }
    }
}
