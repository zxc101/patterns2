using System;
using System.Collections.Generic;
using System.Linq;

namespace Structural
{
    namespace Flyweight
    {
        public class FlyweightFactory
        {
            private List<Tuple<Flyweight, string>> flyweights = new List<Tuple<Flyweight, string>>();
            public FlyweightFactory(params Car[] args)
            {
                for(int i = 0; i < args.Length; i++)
                {
                    flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(args[i]), GetKey(args[i])));
                }
            }
            public string GetKey(Car key)
            {
                List<string> elements = new List<string>();

                elements.Add(key.Model);
                elements.Add(key.Color);
                elements.Add(key.Company);

                if(key.Owner != null && key.Number != null)
                {
                    elements.Add(key.Owner);
                    elements.Add(key.Number);
                }

                elements.Sort();

                return string.Join("_", elements);
            }
            public Flyweight GetFlyweight(Car sharedState)
            {
                string key = GetKey(sharedState);
                if(flyweights.Where(t => t.Item2 == key).Count() == 0)
                {
                    flyweights.Add(new Tuple<Flyweight, string>(new Flyweight(sharedState), key));
                }
                return flyweights.Where(t => t.Item2 == key).FirstOrDefault().Item1;
            }
            public string ListFlyweights()
            {
                var count = flyweights.Count;
                string res = $"FlyweightFactory: I have {count} flyweights:\n";
                for(int i = 0; i < count; i++)
                {
                    res += $"{flyweights[i].Item2}\n";
                }
                return res;
            }
        }
    }
}
