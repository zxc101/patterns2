using Newtonsoft.Json;

namespace Structural
{
    namespace Flyweight
    {
        public class Flyweight
        {
            private Car sharesState;
            public Flyweight(Car car)
            {
                sharesState = car;
            }

            public string Operation(Car uniqueState)
            {
                string s = JsonConvert.SerializeObject(sharesState);
                string u = JsonConvert.SerializeObject(uniqueState);
                return $"Flyweight: Displaying shared {s} and unique {u} state.";
            }
        }
    }
}
