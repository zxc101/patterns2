namespace Structural
{
    namespace Bridge
    {
        public class Abstruction
        {
            protected IImplementation implementation;

            public Abstruction(IImplementation Implementation)
            {
                implementation = Implementation;
            }

            public virtual string Operation()
            {
                return "Abstruct: Base operation with:\n" +
                    implementation.OperationImplementation();
            }
        }
    }
}
