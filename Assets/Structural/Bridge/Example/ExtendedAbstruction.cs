namespace Structural
{
    namespace Bridge
    {
        public class ExtendedAbstruction : Abstruction
        {
            public ExtendedAbstruction(IImplementation implementation) : base(implementation) { }

            public override string Operation()
            {
                return "ExtendedAbstraction: Extended operation with:\n" +
                        implementation.OperationImplementation();
            }
        }
    }
}
