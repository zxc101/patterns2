namespace Structural
{
    namespace Bridge
    {
        public interface IImplementation
        {
            string OperationImplementation();
        }
    }
}
