using UnityEngine;

namespace Structural
{
    namespace Bridge
    {
        public class Example : MonoBehaviour
        {
            // Start is called before the first frame update
            void Start()
            {
                Abstruction abstruction;

                abstruction = new Abstruction(new ConcreteImplementationA());
                ClientCode(abstruction);

                print("\n");

                abstruction = new ExtendedAbstruction(new ConcreteImplementationB());
                ClientCode(abstruction);
            }

            public void ClientCode(Abstruction abstruction)
            {
                print(abstruction.Operation());
            }
        }
    }
}
