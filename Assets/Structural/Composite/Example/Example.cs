using UnityEngine;

namespace Structural
{
    namespace Composite
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Client client = new Client();

                Leaf leaf = new Leaf();
                print("Client: I get a simple component:\n");
                print(client.ClientCode(leaf) + "\n");

                Composite tree = new Composite();
                Composite branch1 = new Composite();
                branch1.Add(new Leaf());
                branch1.Add(new Leaf());
                Composite branch2 = new Composite();
                branch2.Add(new Leaf());
                tree.Add(branch1);
                tree.Add(branch2);
                print("Client: Now I've got a composite tree:\n");
                print(client.ClientCode(tree) + "\n");

                print("Client: I don't need to check the components classes even when managing the tree:\n");
                print(client.ClientCode2(tree, leaf) + "\n");
            }
        }
    }
}
