using System.Collections.Generic;

namespace Structural
{
    namespace Composite
    {
        public class Composite : MyComponent
        {
            protected List<MyComponent> children = new List<MyComponent>();
            public override void Add(MyComponent component)
            {
                children.Add(component);
            }
            public override void Remove(MyComponent component)
            {
                children.Remove(component);
            }
            public override string Operation()
            {
                string result = "Branch( ";
                for (int i = 0; i < children.Count; i++)
                {
                    result += children[i].Operation();
                    if (i != children.Count - 1)
                    {
                        result += " + ";
                    }
                }
                return result + " )";
            }
        }
    }
}
