namespace Structural
{
    namespace Composite
    {
        public class Client
        {
            public string ClientCode(MyComponent leaf)
            {
                return $"RESULT: {leaf.Operation()}";
            }
            public string ClientCode2(MyComponent component1, MyComponent component2)
            {
                if (component1.isComposite())
                {
                    component1.Add(component2);
                }

                return $"RESULT: {component1.Operation()}";
            }
        }
    }
}
