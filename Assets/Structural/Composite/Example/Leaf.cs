namespace Structural
{
    namespace Composite
    {
        public class Leaf : MyComponent
        {
            public override string Operation()
            {
                return "Leaf";
            }

            public override bool isComposite()
            {
                return false;
            }
        }
    }
}
