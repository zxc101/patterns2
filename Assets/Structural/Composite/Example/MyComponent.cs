namespace Structural
{
    namespace Composite
    {
        public abstract class MyComponent
        {
            public MyComponent() { }
            public abstract string Operation();
            public virtual void Add(MyComponent component)
            {
                throw new System.NotImplementedException();
            }
            public virtual void Remove(MyComponent component)
            {
                throw new System.NotImplementedException();
            }
            public virtual bool isComposite()
            {
                return true;
            }
        }
    }
}
