using UnityEngine;

namespace Structural
{
    namespace Decorator
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                var simple = new ConcreteComponent();
                print("Client: I get a simple component:\n");
                ClientCode(simple);
                
                print("\n");

                ConcreteDecoratorA decorator1 = new ConcreteDecoratorA(simple);
                ConcreteDecoratorB decorator2 = new ConcreteDecoratorB(decorator1);
                print("Client: Now I've got a decorated component:\n");
                ClientCode(decorator2);
            }

            public void ClientCode(Component component)
            {
                print($"RESULT: {component.Operation()}\n");
            }
        }
    }
}
