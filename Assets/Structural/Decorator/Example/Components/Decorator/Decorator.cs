namespace Structural
{
    namespace Decorator
    {
        public class Decorator : Component
        {
            public Component Component { set => _component = value; }
            protected Component _component;
            public Decorator(Component component)
            {
                _component = component;
            }
            public override string Operation()
            {
                if(_component != null)
                {
                    return _component.Operation();
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}
