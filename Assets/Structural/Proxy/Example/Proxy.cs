namespace Structural
{
    namespace Proxy
    {
        public class Proxy : ISubject
        {
            private RealSubject realSubject;
            public Proxy(RealSubject RealSubject)
            {
                realSubject = RealSubject;
            }
            public void Request(ref string str)
            {
                if(CheckAccess(ref str))
                {
                    realSubject.Request(ref str);
                    LogAccess(ref str);
                }
            }
            public bool CheckAccess(ref string str)
            {
                str += "Proxy: Chacking access prior to firing a real request.\n";
                return true;
            }
            public void LogAccess(ref string str)
            {
                str += "Proxy: Ligging the time of request.\n";
            }
        }
    }
}
