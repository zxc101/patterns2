using UnityEngine;

namespace Structural
{
    namespace Proxy
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                print("Client: Executing the client code with a real subject:\n");
                RealSubject realSubject = new RealSubject();
                ClientCode(realSubject);

                print("\n");

                print("Client: Executing the same client code with a proxy:\n");
                Proxy proxy = new Proxy(realSubject);
                ClientCode(proxy);
            }

            public void ClientCode(ISubject subject)
            {
                string str = string.Empty;
                subject.Request(ref str);
                print(str);
            }
        }
    }
}
