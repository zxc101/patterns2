namespace Structural
{
    namespace Proxy
    {
        public class RealSubject : ISubject
        {
            public void Request(ref string str)
            {
                str += "RealSubject: Handling Request.\n";
            }
        }
    }
}
