namespace Structural
{
    namespace Proxy
    {
        public interface ISubject
        {
            void Request(ref string str);
        }
    }
}
