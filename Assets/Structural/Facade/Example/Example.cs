using UnityEngine;

namespace Structural
{
    namespace Facade
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Subsystem1 subsystem1 = new Subsystem1();
                Subsystem2 subsystem2 = new Subsystem2();
                Facade facade = new Facade(subsystem1, subsystem2);
                print(facade.Operation());
            }
        }
    }
}
