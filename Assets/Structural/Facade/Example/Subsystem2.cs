namespace Structural
{
    namespace Facade
    {
        public class Subsystem2
        {
            public string Operation1()
            {
                return "Subsystem2: Get ready!\n";
            }
            public string OperationM()
            {
                return "Subsystem2: Fire!\n";
            }
        }
    }
}
