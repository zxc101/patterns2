using UnityEngine;

namespace Structural
{
    namespace Adapter
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Adapter1 adapter1 = new Adapter1();
                ITarget target = new Adapter2(adapter1);

                print("Adapter1 interface is incompatible with the client.\n");
                print("But with Adapter2 client can call it's method\n");

                print(target.GetRequest() + "\n");
            }
        }
    }
}
