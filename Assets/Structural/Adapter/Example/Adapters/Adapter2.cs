namespace Structural
{
    namespace Adapter
    {
        public class Adapter2 : ITarget
        {
            private readonly Adapter1 adapter1;

            public Adapter2(Adapter1 _adapter1)
            {
                adapter1 = _adapter1;
            }

            public string GetRequest()
            {
                return $"This is '{adapter1.GetSpecificRequest()}'";
            }
        }
    }
}
