namespace Structural
{
    namespace Adapter
    {
        public class Adapter1
        {
            public string GetSpecificRequest()
            {
                return "Specific request.";
            }
        }
    }
}
