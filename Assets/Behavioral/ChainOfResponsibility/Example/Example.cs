using System.Collections.Generic;
using UnityEngine;

namespace Behavioral
{
    namespace CoR
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                var monkey = new MonkeyHandler();
                var squirrel = new SquirrelHandler();
                var dog = new DogHandler();

                monkey.SetNext(squirrel).SetNext(dog);

                print($"Chain: Monkey > Squirrel > Dog\n");
                ClientCode(monkey);

                print("\n");

                print("Subchain: Squirrel > Dog\n");
                ClientCode(squirrel);
            }

            public static void ClientCode(AbstractHandler handler )
            {
                foreach (var food in new List<string> { "Nut", "MeatBall", "Banana" })
                {
                    print($"Client: Who wants a {food}?\n");

                    var result = handler.Handle(food);

                    if(result != null)
                    {
                        print($"{result}\n");
                    }
                    else
                    {
                        print($"{food} was left untouched\n");
                    }
                }
            }
        }
    }
}
