using System.Collections.Generic;

namespace Behavioral
{
    namespace Strategy
    {
        public class Context
        {
            private IStrategy _strategy;
            public Context() { }
            public Context(IStrategy strategy)
            {
                _strategy = strategy;
            }
            public void SetStrategy(IStrategy strategy)
            {
                _strategy = strategy;
            }
            public void DoSomeBusinessLogic(ref string log)
            {
                log += "Context: Sorting data using the strategy (not sure how it'll do it)\n";
                object result = _strategy.DoAlgorithm(new List<string> { "a", "b", "c", "d", "e" });

                string resultStr = string.Empty;
                foreach (var element in result as List<string>)
                {
                    resultStr += element + ", ";
                }

                log += resultStr + "\n";
            }
        }
    }
}
