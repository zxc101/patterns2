using UnityEngine;

namespace Behavioral
{
    namespace Strategy
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                var context = new Context();
                string log = string.Empty;

                print("Client: Strategy is set to normal sorting.\n");
                context.SetStrategy(new ConcreteStrategyA());
                context.DoSomeBusinessLogic(ref log);
                Print(ref log);

                print("\n");

                print("Client: Strategy is set to reverse sorting.\n");
                context.SetStrategy(new ConcreteStrategyB());
                context.DoSomeBusinessLogic(ref log);
                Print(ref log);
            }

            private void Print(ref string log)
            {
                print(log + "\n");
                log = string.Empty;
            }
        }
    }
}
