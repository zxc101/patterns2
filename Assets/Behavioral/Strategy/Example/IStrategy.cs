namespace Behavioral
{
    namespace Strategy
    {
        public interface IStrategy
        {
            object DoAlgorithm(object data);
        }
    }
}
