using UnityEngine;

namespace Behavioral
{
    namespace Memento
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                string str = string.Empty;
                Originator originator = new Originator("Super-duper-super-puper-super.", ref str);
                Caretaker caretaker = new Caretaker(originator);

                caretaker.Backup(ref str);
                originator.DoSomething(ref str);
                Print(ref str);

                caretaker.Backup(ref str);
                originator.DoSomething(ref str);
                Print(ref str);

                caretaker.Backup(ref str);
                originator.DoSomething(ref str);
                Print(ref str);

                print("\n");

                caretaker.ShowHistory(ref str);
                Print(ref str);

                print("Client: Now let's rollback\n");
                caretaker.Undo(ref str);
                Print(ref str);

                print("\nClient: Once more!\n");
                caretaker.Undo(ref str);
                Print(ref str);

                print("\nClient: Original!\n");
                caretaker.Undo(ref str);
                Print(ref str);
            }

            private void Print(ref string str)
            {
                print(str);
                str = string.Empty;
            }
        }
    }
}
