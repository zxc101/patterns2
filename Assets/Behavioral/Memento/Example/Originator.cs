using System.Threading;

namespace Behavioral
{
    namespace Memento
    {
        public class Originator
        {
            private string _state;

            public Originator(string state, ref string str)
            {
                _state = state;
                str += $"Originator: My initial state is: {state}\n";
            }

            public void DoSomething(ref string str)
            {
                str += "Originator: I'm doing something important.\n";
                _state = GenerateRandomString(30);
                str += $"Originator: and my state has changed to: {_state}\n";
            }

            private string GenerateRandomString(int length = 30)
            {
                string allowedSymbols = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string result = string.Empty;

                while(length > 0)
                {
                    result += allowedSymbols[UnityEngine.Random.Range(0, allowedSymbols.Length)];
                    Thread.Sleep(12);
                    length--;
                }

                return result;
            }

            public IMemento Save()
            {
                return new ConcreteMemento(_state);
            }

            public void Restore(IMemento memento, ref string str)
            {
                if(!(memento is ConcreteMemento))
                {
                    str += $"Unknown memento class {memento}\n";
                    return;
                }

                _state = memento.GetState();
                str += $"Originator: My state has Changed to: {_state}\n";
            }
        }
    }
}
