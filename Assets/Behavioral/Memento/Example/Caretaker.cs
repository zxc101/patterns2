using System.Collections.Generic;

namespace Behavioral
{
    namespace Memento
    {
        public class Caretaker
        {
            private List<IMemento> _mementos = new List<IMemento>();
            private Originator _originator = null;

            public Caretaker(Originator originator)
            {
                _originator = originator;
            }

            public void Backup(ref string str)
            {
                str += "Caretaker: Saving Originator's state...\n";
                _mementos.Add(_originator.Save());
            }

            public void Undo(ref string str)
            {
                if(_mementos.Count == 0)
                {
                    return;
                }

                var memento = _mementos[_mementos.Count - 1];
                _mementos.Remove(memento);

                str += $"Caretaker: Restoring state to: {memento.GetName()}\n";

                _originator.Restore(memento, ref str);
            }

            public void ShowHistory(ref string str)
            {
                str += "Caretaker: Here's the list of mementos:\n";
                foreach(var memento in _mementos)
                {
                    str += memento.GetName() + "\n";
                }
            }
        }
    }
}
