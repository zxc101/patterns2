using System.Collections;

namespace Behavioral
{
    namespace Iterator
    {
        public abstract class MyIterator : IEnumerator
        {
            object IEnumerator.Current => Current();
            public abstract int Key();
            public abstract object Current();
            public abstract bool MoveNext();
            public abstract void Reset();
        }
    }
}
