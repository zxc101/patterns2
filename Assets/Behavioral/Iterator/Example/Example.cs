using UnityEngine;

namespace Behavioral
{
    namespace Iterator
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                var collection = new WordsCollection<string>();
                collection.AddItem("First");
                collection.AddItem("Second");
                collection.AddItem("Third");

                print("Straight traversal:\n");

                foreach(string element in collection)
                {
                    print(element + "\n");
                }

                print("\n");

                print("Reverse traversal:\n");

                collection.ReverceDirection();

                foreach(string element in collection)
                {
                    print(element + "\n");
                }
            }
        }
    }
}
