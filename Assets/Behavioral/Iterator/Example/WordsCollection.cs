using System.Collections;
using System.Collections.Generic;

namespace Behavioral
{
    namespace Iterator
    {
        public class WordsCollection<T> : IteratorAggregate
        {
            List<T> _collection = new List<T>();
            bool _direction = false;
            public void ReverceDirection()
            {
                _direction = !_direction;
            }
            public List<T> GetItems()
            {
                return _collection;
            }
            public void AddItem(T item)
            {
                _collection.Add(item);
            }
            public override IEnumerator GetEnumerator()
            {
                return new AlphabeticalOrderIterator<T>(this, _direction);
            }
        }
    }
}
