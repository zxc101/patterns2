using System.Collections;

namespace Behavioral
{
    namespace Iterator
    {
        public abstract class IteratorAggregate : IEnumerable
        {
            public abstract IEnumerator GetEnumerator();
        }
    }
}
