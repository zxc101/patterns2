namespace Behavioral
{
    namespace Observer
    {
        public class ConcreteObserverA : IObserver
        {
            public void Update(ISubject subject, ref string str)
            {
                if((subject as Subject).State < 3)
                {
                    str += "ConcreteObserverA: Reacted to the event.\n";
                }
            }
        }
    }
}
