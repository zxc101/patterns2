namespace Behavioral
{
    namespace Observer
    {
        public class ConcreteObserverB : IObserver
        {
            public void Update(ISubject subject, ref string str)
            {
                if((subject as Subject).State == 0 || (subject as Subject).State >= 2)
                {
                    str += "ConcreteObserverB: Reacted to the event.\n";
                }
            }
        }
    }
}
