namespace Behavioral
{
    namespace Observer
    {
        public interface ISubject
        {
            void Attach(IObserver observer, ref string str);
            void Detach(IObserver observer, ref string str);
            void Notify(ref string str);
        }
    }
}
