namespace Behavioral
{
    namespace Observer
    {
        public interface IObserver
        {
            void Update(ISubject subject, ref string str);
        }
    }
}
