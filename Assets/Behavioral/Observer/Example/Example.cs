using UnityEngine;

namespace Behavioral
{
    namespace Observer
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                string str = string.Empty;
                
                var subject = new Subject();

                var observerA = new ConcreteObserverA();
                subject.Attach(observerA, ref str);

                var observerB = new ConcreteObserverB();
                subject.Attach(observerB, ref str);
                Print(ref str);

                subject.SomeBusinessLogic(ref str);
                Print(ref str);
                subject.SomeBusinessLogic(ref str);

                subject.Detach(observerB, ref str);
                Print(ref str);

                subject.SomeBusinessLogic(ref str);
                Print(ref str);
            }

            private void Print(ref string str)
            {
                print(str);
                str = string.Empty;
            }
        }
    }
}
