using System.Collections.Generic;

namespace Behavioral
{
    namespace Observer
    {
        public class Subject : ISubject
        {
            public int State { get; set; } = -0;
            private List<IObserver> observers = new List<IObserver>();
            public void Attach(IObserver observer, ref string str)
            {
                str += "Subject: Attached an observer.\n";
                observers.Add(observer);
            }
            public void Detach(IObserver observer, ref string str)
            {
                observers.Remove(observer);
                str += "Subject: Detached an observer.\n";
            }
            public void Notify(ref string str)
            {
                str += "Subject: Notifying observers...\n";

                for(int i = 0; i < observers.Count; i++)
                {
                    observers[i].Update(this, ref str);
                }
            }
            public void SomeBusinessLogic(ref string str)
            {
                str += "Subject: I'm doing something important.\n";
                State = UnityEngine.Random.Range(0, 10);

                str += $"Subject: My state has just changed to: {State}\n";
                Notify(ref str);
            }
        }
    }
}
