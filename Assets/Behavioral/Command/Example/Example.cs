using UnityEngine;

namespace Behavioral
{
    namespace Command
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Invoker invoker = new Invoker();
                invoker.SetOnStart(new SimpleCommand("Say Hi!"));
                Receiver receiver = new Receiver();
                invoker.SetOnFinish(new ComplexCommand(receiver, "Sand email", "Save report"));

                string str = string.Empty;
                invoker.DoSomethingImportant(ref str);
                print(str);
            }
        }
    }
}
