namespace Behavioral
{
    namespace Command
    {
        public class Invoker
        {
            private ICommand _onStart;
            private ICommand _onFinish;

            public void SetOnStart(ICommand command)
            {
                _onStart = command;
            }

            public void SetOnFinish(ICommand command)
            {
                _onFinish = command;
            }

            public void DoSomethingImportant(ref string str)
            {
                str += "Invoker: Does anybody want something done before I begin?\n";
                if(_onStart is ICommand)
                {
                    _onStart.Execute(ref str);
                }

                str += "Invoker: ...doing something really important...\n";

                str += "Invoker: Does anybody want something done after I finished?\n";
                if(_onFinish is ICommand)
                {
                    _onFinish.Execute(ref str);
                }
            }
        }
    }
}
