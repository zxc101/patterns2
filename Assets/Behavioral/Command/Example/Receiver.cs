namespace Behavioral
{
    namespace Command
    {
        public class Receiver
        {
            public void DoSomething(ref string str, string a)
            {
                str += $"Receiver: Working on ({a}.)\n";
            }

            public void DoSomethingElse(ref string str, string b)
            {
                str += $"Receiver: Also working on ({b}.)\n";
            }
        }
    }
}
