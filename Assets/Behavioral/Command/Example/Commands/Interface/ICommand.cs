namespace Behavioral
{
    namespace Command
    {
        public interface ICommand
        {
            void Execute(ref string log);
        }
    }
}
