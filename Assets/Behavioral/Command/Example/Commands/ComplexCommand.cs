namespace Behavioral
{
    namespace Command
    {
        public class ComplexCommand : ICommand
        {
            private Receiver _receiver;

            private string _a;

            private string _b;

            public ComplexCommand(Receiver receiver, string a, string b)
            {
                _receiver = receiver;
                _a = a;
                _b = b;
            }

            public void Execute(ref string log)
            {
                log += "ComplexCommand: Complex stuff should be done by a receiver object.\n";
                _receiver.DoSomething(ref log, _a);
                _receiver.DoSomethingElse(ref log, _b);
            }
        }
    }
}
