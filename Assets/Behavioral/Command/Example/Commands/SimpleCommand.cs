namespace Behavioral
{
    namespace Command
    {
        public class SimpleCommand : ICommand
        {
            private string _payload = string.Empty;
            public SimpleCommand(string payload)
            {
                _payload = payload;
            }
            public void Execute(ref string log)
            {
                log += $"SimpleCommand: See, I can do simple things like printing ({_payload})\n";
            }
        }
    }
}
