using UnityEngine;

namespace Behavioral
{
    namespace TemplateMethod
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                print("Same client code can work with different subclasses:\n");
                ClientCode(new ConcreteClass1());

                print("\n");

                print("Same client code can work with different subclasses:\n");
                ClientCode(new ConcreteClass2());
            }

            public static void ClientCode(AbstructClass abstructClass)
            {
                string log = string.Empty;
                abstructClass.TemplateMethod(ref log);
                print(log + "\n");
            }
        }
    }
}
