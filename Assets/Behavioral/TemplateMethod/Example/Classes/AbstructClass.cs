namespace Behavioral
{
    namespace TemplateMethod
    {
        public abstract class AbstructClass
        {
            public void TemplateMethod(ref string log)
            {
                BaseOperation1(ref log);
                RequiredOperations1(ref log);
                BaseOperation2(ref log);
                Hook1(ref log);
                RequiredOperations2(ref log);
                BaseOperation3(ref log);
                Hook2(ref log);
            }

            protected void BaseOperation1(ref string log)
            {
                log += "AbstractClass says: I am doing the bulk of the work\n";
            }

            protected void BaseOperation2(ref string log)
            {
                log += "AbstractClass says: I let subclasses override some operations\n";
            }

            protected void BaseOperation3(ref string log)
            {
                log += "AbstractClass says: But I am doing the bulk of the work anyway\n";
            }

            protected abstract void RequiredOperations1(ref string log);
            protected abstract void RequiredOperations2(ref string log);
            protected virtual void Hook1(ref string log) { }
            protected virtual void Hook2(ref string log) { }
        }
    }
}
