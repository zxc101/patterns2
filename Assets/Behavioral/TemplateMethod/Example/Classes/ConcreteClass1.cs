namespace Behavioral
{
    namespace TemplateMethod
    {
        public class ConcreteClass1 : AbstructClass
        {
            protected override void RequiredOperations1(ref string log)
            {
                log += "ConcreteClass1 says: Implemented Operation1\n";
            }

            protected override void RequiredOperations2(ref string log)
            {
                log += "ConcreteClass1 says: Implemented Operation2\n";
            }
        }
    }
}
