namespace Behavioral
{
    namespace TemplateMethod
    {
        public class ConcreteClass2 : AbstructClass
        {
            protected override void RequiredOperations1(ref string log)
            {
                log += "ConcreteClass2 says: Implemented Operation1\n";
            }
            protected override void RequiredOperations2(ref string log)
            {
                log += "ConcreteClass2 says: Implemented Operation2\n";
            }
            protected override void Hook1(ref string log)
            {
                log += "ConcreteClass2 says: Overridden Hook1\n";
            }
        }
    }
}
