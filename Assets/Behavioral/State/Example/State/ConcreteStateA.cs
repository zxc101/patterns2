namespace Behavioral
{
    namespace State
    {
        public class ConcreteStateA : AState
        {
            public override void Handle1(ref string str)
            {
                str += "ConcreteStateA handles request1.\n";
                str += "ConcreteStateA wants to changed the state of the context.\n";
                _context.TransitionTo(new ConcreteStateB(), ref str);
            }

            public override void Handle2(ref string str)
            {
                str += "ConcreteStateA handles request2.\n";
            }
        }
    }
}
