namespace Behavioral
{
    namespace State
    {
        public abstract class AState
        {
            protected Context _context;

            public void SetContext(Context context)
            {
                _context = context;
            }

            public abstract void Handle1(ref string str);
            public abstract void Handle2(ref string str);
        }
    }
}
