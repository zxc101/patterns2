namespace Behavioral
{
    namespace State
    {
        public class ConcreteStateB : AState
        {
            public override void Handle1(ref string str)
            {
                str += "ConcreteStateB handles request1.\n";
            }

            public override void Handle2(ref string str)
            {
                str += "ConcreteStateB handles request2.\n";
                str += "ConcreteStateB wants to change the state of the context.\n";
                _context.TransitionTo(new ConcreteStateA(), ref str);
            }
        }
    }
}
