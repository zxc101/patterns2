using UnityEngine;

namespace Behavioral
{
    namespace State
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                string str = string.Empty;

                var context = new Context(new ConcreteStateA(), ref str);
                context.Request1(ref str);
                context.Request2(ref str);
                print(str + "\n");
            }
        }
    }
}
