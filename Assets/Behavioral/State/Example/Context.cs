namespace Behavioral
{
    namespace State
    {
        public class Context
        {
            private AState _state = null;

            public Context(AState state, ref string str)
            {
                TransitionTo(state, ref str);
            }

            public void TransitionTo(AState state, ref string str)
            {
                str += $"Context: Transition to {state.GetType().Name}\n";
                _state = state;
                _state.SetContext(this);
            }

            public void Request1(ref string str)
            {
                _state.Handle1(ref str);
            }

            public void Request2(ref string str)
            {
                _state.Handle2(ref str);
            }
        }
    }
}
