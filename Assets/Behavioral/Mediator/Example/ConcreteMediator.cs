namespace Behavioral
{
    namespace Mediator
    {
        public class ConcreteMediator : IMediator
        {
            private Component1 _component1;
            private Component2 _component2;
            public ConcreteMediator(Component1 component1, Component2 component2)
            {
                _component1 = component1;
                _component2 = component2;

                _component1.SetMediator(this);
                _component2.SetMediator(this);
            }
            public void Notify(object sender, string ev, ref string str)
            {
                if(ev == "A")
                {
                    str += "Mediator reacts on A and triggers folowing operation:\n";
                    _component2.DoC(ref str);
                }
                if(ev == "D")
                {
                    str += "Mediator reacts on D and triggers following operations:\n";
                    _component1.DoB(ref str);
                    _component2.DoC(ref str);
                }
            }
        }
    }
}
