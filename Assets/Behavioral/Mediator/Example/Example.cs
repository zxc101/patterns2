using UnityEngine;

namespace Behavioral
{
    namespace Mediator
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Component1 component1 = new Component1();
                Component2 component2 = new Component2();
                new ConcreteMediator(component1, component2);

                string str = string.Empty;

                print("Client triggers operation A.\n");
                component1.DoA(ref str);
                print(str);

                print("\n");

                str = string.Empty;
                print("Client triggers operation D.\n");
                component2.DoD(ref str);
                print(str);
            }
        }
    }
}
