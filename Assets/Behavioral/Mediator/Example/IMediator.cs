namespace Behavioral
{
    namespace Mediator
    {
        public interface IMediator
        {
            void Notify(object sender, string ev, ref string str);
        }
    }
}
