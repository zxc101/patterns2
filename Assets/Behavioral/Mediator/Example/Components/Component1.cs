namespace Behavioral
{
    namespace Mediator
    {
        public class Component1 : BaseComponent
        {
            public void DoA(ref string str)
            {
                str += "Component 1 does A.\n";
                _mediator.Notify(this, "A", ref str);
            }
            public void DoB(ref string str)
            {
                str += "Component 1 does B.\n";
                _mediator.Notify(this, "B", ref str);
            }
        }
    }
}
