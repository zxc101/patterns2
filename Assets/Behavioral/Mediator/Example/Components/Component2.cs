namespace Behavioral
{
    namespace Mediator
    {
        public class Component2 : BaseComponent
        {
            public void DoC(ref string str)
            {
                str += "Component 2 does C.\n";
                _mediator.Notify(this, "C", ref str);
            }
            public void DoD(ref string str)
            {
                str += "Component 2 does D.\n";
                _mediator.Notify(this, "D", ref str);
            }
        }
    }
}
