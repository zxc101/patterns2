namespace Behavioral
{
    namespace Visitor
    {
        public interface IComponent
        {
            void Accept(IVisitor visitor, ref string log);
        }
    }
}
