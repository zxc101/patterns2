namespace Behavioral
{
    namespace Visitor
    {
        public class ConcreteComponentA : IComponent
        {
            public void Accept(IVisitor visitor, ref string log)
            {
                visitor.VisitConcreteComponentA(this, ref log);
            }

            public string ExclusiveMethodOfConcreteComponentA()
            {
                return "A";
            }
        }
    }
}
