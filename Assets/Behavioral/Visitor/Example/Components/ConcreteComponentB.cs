namespace Behavioral
{
    namespace Visitor
    {
        public class ConcreteComponentB : IComponent
        {
            public void Accept(IVisitor visitor, ref string log)
            {
                visitor.VisitConcreteComponentB(this, ref log);
            }

            public string SpecialMethodOfConcreteComponentB()
            {
                return "B";
            }
        }
    }
}
