using System.Collections.Generic;
using UnityEngine;

namespace Behavioral
{
    namespace Visitor
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                string log = string.Empty;

                List<IComponent> components = new List<IComponent>
                {
                    new ConcreteComponentA(),
                    new ConcreteComponentB()
                };

                print("The client code works with all visitors via the base Visitor interface:\n");
                var visitor1 = new ConcreteVisitor1();
                ClientCode(components, visitor1, ref log);
                
                print("\n");

                print("It allows the same client code to work with different types of visitors:\n");
                var visitor2 = new ConcreteVisitor2();
                ClientCode(components, visitor2, ref log);
            }

            private void ClientCode(List<IComponent> components, IVisitor visitor, ref string log)
            {
                for (int i = 0; i < components.Count; i++)
                {
                    components[i].Accept(visitor, ref log);
                }
                Print(ref log);
            }

            private void Print(ref string log)
            {
                print(log);
                log = string.Empty;
            }
        }
    }
}
