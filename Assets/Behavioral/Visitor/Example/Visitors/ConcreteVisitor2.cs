namespace Behavioral
{
    namespace Visitor
    {
        public class ConcreteVisitor2 : IVisitor
        {
            public void VisitConcreteComponentA(ConcreteComponentA element, ref string log)
            {
                log += $"{element.ExclusiveMethodOfConcreteComponentA()} ConcreteVisitor2\n";
            }

            public void VisitConcreteComponentB(ConcreteComponentB element, ref string log)
            {
                log += $"{element.SpecialMethodOfConcreteComponentB()} ConcreteVisitor2\n";
            }
        }
    }
}
