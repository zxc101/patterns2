namespace Behavioral
{
    namespace Visitor
    {
        public class ConcreteVisitor1 : IVisitor
        {
            public void VisitConcreteComponentA(ConcreteComponentA element, ref string log)
            {
                log += $"{element.ExclusiveMethodOfConcreteComponentA()} ConcreteVisitor1\n";
            }

            public void VisitConcreteComponentB(ConcreteComponentB element, ref string log)
            {
                log += $"{element.SpecialMethodOfConcreteComponentB()} ConcreteVisitor1\n";
            }
        }
    }
}
