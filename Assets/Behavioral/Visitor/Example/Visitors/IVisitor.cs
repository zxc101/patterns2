using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Behavioral
{
    namespace Visitor
    {
        public interface IVisitor
        {
            void VisitConcreteComponentA(ConcreteComponentA element, ref string log);
            void VisitConcreteComponentB(ConcreteComponentB element, ref string log);
        }
    }
}
