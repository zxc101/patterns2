namespace Generative
{
    namespace Builder
    {
        public interface IBuilder
        {
            void BuilderPartA();
            void BuilderPartB();
            void BuilderPartC();
        }
    }
}
