using UnityEngine;

namespace Generative
{
    namespace Builder
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Director director = new Director();
                ConcreteBuilder builder = new ConcreteBuilder();

                director.Builder = builder;

                print("Standard basic product:\n");
                director.BuildMinimalViableProduct();
                print(builder.GetProduct().ListParts());

                print("Standard full featured product:\n");
                director.BuildFullFeaturedProduct();
                print(builder.GetProduct().ListParts());

                print("Custom product:\n");
                builder.BuilderPartA();
                builder.BuilderPartC();
                print(builder.GetProduct().ListParts());
            }
        }
    }
}

