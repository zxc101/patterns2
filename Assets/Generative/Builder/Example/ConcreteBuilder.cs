namespace Generative
{
    namespace Builder
    {
        public class ConcreteBuilder : IBuilder
        {
            private Product product = new Product();

            public ConcreteBuilder()
            {
                Reset();
            }

            public void Reset()
            {
                product = new Product();
            }

            public void BuilderPartA()
            {
                product.Add("PartA1");
            }

            public void BuilderPartB()
            {
                product.Add("PartB1");
            }

            public void BuilderPartC()
            {
                product.Add("PartC1");
            }

            public Product GetProduct()
            {
                Product result = product;

                Reset();

                return result;
            }
        }
    }
}
