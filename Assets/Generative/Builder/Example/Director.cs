namespace Generative
{
    namespace Builder
    {
        public class Director
        {
            private IBuilder builder;

            public IBuilder Builder
            {
                set { builder = value; }
            }

            public void BuildMinimalViableProduct()
            {
                builder.BuilderPartA();
            }

            public void BuildFullFeaturedProduct()
            {
                builder.BuilderPartA();
                builder.BuilderPartB();
                builder.BuilderPartC();
            }
        }
    }
}
