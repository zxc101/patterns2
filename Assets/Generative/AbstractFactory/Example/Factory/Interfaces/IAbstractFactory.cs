namespace Generative
{
    namespace AbstractFactory
    {
        public interface IAbstractFactory
        {
            IAbstractProductA CreateProductA();
            IAbstratcProductB CreateProductB();
        }
    }
}
