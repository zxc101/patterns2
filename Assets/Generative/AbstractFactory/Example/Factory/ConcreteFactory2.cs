namespace Generative
{
    namespace AbstractFactory
    {
        public class ConcreteFactory2 : IAbstractFactory
        {
            public IAbstractProductA CreateProductA()
            {
                return new ConcreteProductA2();
            }

            public IAbstratcProductB CreateProductB()
            {
                return new ConcreteProductB2();
            }
        }
    }
}
