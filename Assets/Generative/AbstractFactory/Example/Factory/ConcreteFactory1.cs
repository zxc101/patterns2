namespace Generative
{
    namespace AbstractFactory
    {
        public class ConcreteFactory1 : IAbstractFactory
        {
            public IAbstractProductA CreateProductA()
            {
                return new ConcreteProductA1();
            }

            public IAbstratcProductB CreateProductB()
            {
                return new ConcreteProductB1();
            }
        }
    }
}
