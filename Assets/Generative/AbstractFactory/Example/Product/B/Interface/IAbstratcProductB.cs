namespace Generative
{
    namespace AbstractFactory
    {
        public interface IAbstratcProductB
        {
            string UsefulFunctionB();

            string AnotherUsefulFunctionB(IAbstractProductA collaborator);
        }
    }
}
