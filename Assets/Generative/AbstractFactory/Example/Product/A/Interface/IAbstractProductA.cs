namespace Generative
{
    namespace AbstractFactory
    {
        public interface IAbstractProductA
        {
            string UsefulFunctionA();
        }
    }
}
