using UnityEngine;

namespace Generative
{
    namespace AbstractFactory
    {
        public class Example : MonoBehaviour
        {
            private void Start()
            {
                print("Client: Testing client code with the first factory type...\n");
                ClientMethod(new ConcreteFactory1());

                print("\n");

                print("Client: Testing the same client code with the second factory type...\n");
                ClientMethod(new ConcreteFactory2());
            }

            public void ClientMethod(IAbstractFactory factory)
            {
                var productA = factory.CreateProductA();
                var productB = factory.CreateProductB();

                print(productB.UsefulFunctionB() + "\n");
                print(productB.AnotherUsefulFunctionB(productA) + "\n");
            }
        }
    }
}
