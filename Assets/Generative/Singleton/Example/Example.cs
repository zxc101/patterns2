using UnityEngine;

namespace Generative
{
    namespace Singleton
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Singleton s1 = Singleton.Instance;
                Singleton s2 = Singleton.Instance;

                if (s1 == s2)
                {
                    print("s1 == s2\n");
                }
                else
                {
                    print("s1 != s2\n");
                }
            }
        }
    }
}
