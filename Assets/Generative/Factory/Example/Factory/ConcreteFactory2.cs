namespace Generative
{
    namespace Factory
    {
        public class ConcreteFactory2 : Factory
        {
            public override IProduct FactoryMethod()
            {
                return new ConcreteProduct2();
            }
        }
    }
}
