namespace Generative
{
    namespace Factory
    {
        public class ConcreteFactory1 : Factory
        {
            public override IProduct FactoryMethod()
            {
                return new ConcreteProduct1();
            }
        }
    }
}
