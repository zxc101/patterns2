namespace Generative
{
    namespace Factory
    {
        public abstract class Factory
        {
            public abstract IProduct FactoryMethod();

            public string SomeOperation()
            {
                return $"Creator: The some creator's code has just worked with {FactoryMethod().Operation()}\n";
            }
        }
    }
}
