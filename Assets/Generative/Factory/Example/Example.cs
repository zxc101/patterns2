using UnityEngine;

namespace Generative
{
    namespace Factory
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                print("App: Launched with the ConcreteCreator1.\n");
                ClientCode(new ConcreteFactory1());

                print("\n");

                print("App: Launched with the ConcreteCreator2.\n");
                ClientCode(new ConcreteFactory2());
            }

            public void ClientCode(Factory factory)
            {
                print(string.Format("Client I'm not aware of the creator's class, " +
                    "but it still works. \n{0}", factory.SomeOperation()));
            }
        }
    }
}
