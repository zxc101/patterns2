using UnityEngine;

namespace Generative
{
    namespace Prototype
    {
        public class Example : MonoBehaviour
        {
            void Start()
            {
                Person p1 = new Person();
                p1.age = 42;
                p1.birthDay = System.Convert.ToDateTime("1977-01-01");
                p1.name = "Jack Daniels";
                p1.idInfo = new IdInfo(777);

                Person p2 = p1.ShallowCopy();
                Person p3 = p1.DeepCopy();

                print("Original values of p1, p2, p3:\n");
                print("p1 instance values:\n");
                DisplayValues(p1);
                print("p2 instance values:\n");
                DisplayValues(p2);
                print("p3 instance values:\n");
                DisplayValues(p3);

                p1.age = 32;
                p1.birthDay = System.Convert.ToDateTime("1900-01-01");
                p1.name = "Frank";
                p1.idInfo.idNumber = 7878;
                print("Values of p1, p2 and p3 after changes to p1:\n");
                print("p1 onstance values:\n");
                DisplayValues(p1);
                print("p2 instance values (reference values have changed):\n");
                DisplayValues(p2);
                print("p3 instance values (everything was kept the same):\n");
                DisplayValues(p3);
            }

            public static void DisplayValues(Person p)
            {
                print(string.Format("ID: {0} | Name: {1} | Age: {2} | BirthData | {3}/{4}/{5}\n",
                                    p.idInfo.idNumber,
                                    p.name,
                                    p.age,
                                    p.birthDay.Month,
                                    p.birthDay.Day,
                                    p.birthDay.Year));
            }
        }
    }
}
