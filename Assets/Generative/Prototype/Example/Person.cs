namespace Generative
{
    namespace Prototype
    {
        public class Person
        {
            public IdInfo idInfo;
            public string name;
            public int age;
            public System.DateTime birthDay;

            public Person ShallowCopy()
            {
                return (Person)MemberwiseClone();
            }

            public Person DeepCopy()
            {
                Person clone = (Person)MemberwiseClone();
                clone.idInfo = new IdInfo(idInfo.idNumber);
                clone.name = string.Copy(name);
                return clone;
            }
        }
    }
}
